import json
import requests
import uuid
from collections import namedtuple
from jinja2 import Environment, FileSystemLoader
import os
from dotenv import load_dotenv

load_dotenv() 

GITLAB_PAT = os.getenv('GITLAB_PAT')
MLFLOW_HOST = os.getenv('MLFLOW_HOST')
GITLAB_HOST = os.getenv('GITLAB_HOST')
GITLAB_PROJECT_ID = os.getenv('GITLAB_PROJECT_ID')

Endpoint = namedtuple('Endpoint', 'title method path cases', defaults=(None, ) * 4)
Case = namedtuple('Case', 'title params populate_params body populate_body', defaults=(None, ) * 5)
Result = namedtuple('Result', 'method url params body status_code output')
Backend = namedtuple('Backend', 'name base_url token data')

DEFAULT_EXPERIMENT_NAME = f"{uuid.uuid4()}"

MLFLOW = Backend("MLFLOW", f"{MLFLOW_HOST}/api/2.0/mlflow", None, {'experiment_name': DEFAULT_EXPERIMENT_NAME})
GITLAB = Backend("GITLAB", f"{GITLAB_HOST}/api/v4/projects/{GITLAB_PROJECT_ID}/ml/mlflow/api/2.0/mlflow", GITLAB_PAT, {'experiment_name': DEFAULT_EXPERIMENT_NAME})

BACKENDS = [MLFLOW, GITLAB]

ENDPOINTS = [
    Endpoint('Create Experiment', 'POST', '/experiments/create', cases=[
        Case('When name does not yet exist', body={'name': 'populate:experiment_name'}),
        Case('When name already exists', body={'name': 'populate:experiment_name'}),
        Case('When name is missing', body={'yolo': 'experiment_name'}),
    ]),
    Endpoint('Get Experiment By Id', 'GET', '/experiments/get', cases=[
        Case('When id exists', params={"experiment_id": "populate:experiment_id"}),
        Case('When id does not exist', params={"experiment_id": "asasdfsadf"}),
        Case('When id is missing', params={"asdasd": "asasdfsadf"}),
    ]),
    Endpoint('Get Experiment By Name', 'GET', '/experiments/get-by-name', cases=[
        Case('When name exists', params={"experiment_name": "populate:experiment_name"}),
        Case('When name does not exist', params={"experiment_name": "asasdfsadf"}),
        Case('When name is missing', params={"asdasd": "asasdfsadf"}),
    ]),
    Endpoint('Create Run', 'POST', '/runs/create', cases=[
        Case('When experiment exists', body={
                "experiment_id": "populate:experiment_id", "start_time": 1234,
            }),
        Case('When experiment does not exist', body={"experiment_id": "asasdfsadf"}),
        Case('When experiment is not passed', body={"yolo": "asasdfsadf"})
    ]),
    Endpoint('Get Run', 'GET', '/runs/get', cases=[
        Case('When run exists', params={"run_id": "populate:run_id"}),
        Case('When run does not exist', params={"run_id": "asasdfsadf"})
    ]),
    Endpoint('Update Run', 'POST', '/runs/update', cases=[
        Case('When run exists', body={"run_id": "populate:run_id", "status": "FAILED", "end_time": 12345678}),
        Case('When run does not exist', body={"run_id": "asasdfsadf"}),
        Case('When run exists, but state is invalid', body={"run_id": "populate:run_id", "status": "YOLO"})
    ]),
    Endpoint('Log Metric', 'POST', '/runs/log-metric', cases=[
        Case('When run exists', body={
            "run_id": "populate:run_id", "key": "hello", "value": 10.0, "timestamp": 12345678, "step": 3
        }),
        Case('When run id is not passed', body={"key": "hello", "value": 10.0, "timestamp": 12345678, "step": 3}),
        Case('When key is not passed', body={"run_id": "populate:run_id", "value": 10.0, "timestamp": 12345678, "step": 3})
    ]),
    Endpoint('Log Params', 'POST', '/runs/log-parameter', cases=[
        Case('When run exists', body={ "run_id": "populate:run_id", "key": "hello", "value": "SomeParameter" }),
        Case('When key is not passed', body={ "run_id": "populate:run_id", "value": "SomeParameter" }),
        Case('When param is repeated', body={ "run_id": "populate:run_id", "key": "hello", "value": "SomeParameter" })
    ]),
    Endpoint('Log Batch', 'POST', '/runs/log-batch', cases=[
        Case('When run exists', body={
            "run_id": "populate:run_id",
            "metrics": [
                { "key": "metric2", "value": 1.0, "timestamp": 12345678, "step": 0 },
                { "key": "metric3", "value": 100.0, "timestamp": 12345678, "step": 0 },
            ],
            "params": [
                { "key": "param1", "value": "ValueForParam1"}
            ]
        }),
        Case('When a metric is passed without key', body={
            "run_id": "populate:run_id",
            "metrics": [
                { "value": 1.0, "timestamp": 12345678, "step": 0 },
                { "key": "metric4", "value": 100.0, "timestamp": 12345678, "step": 0 },
            ]
        }),
        Case('When a parameter is duplicated', body={
            "run_id": "populate:run_id",
            "params": [
                { "key": "param1", "value": "AnotherValue"},
                { "key": "param12", "value": "ValueForParam2"},
            ]
        })
    ]),
    Endpoint('Get Run', 'GET', '/runs/get', cases=[
        Case('When run exists', params={"run_id": "populate:run_id"})
    ])
]

def create_auth(token):
    if token:
        return {"Authorization": f"Bearer {token}"} 

    return {}

def populate(data, original_dict):
    if not original_dict:
        return {} 

    new_dict = {}

    for k,v in original_dict.items():
        if isinstance(v, str) and v.startswith('populate:'):
            new_dict[k] = data[v.removeprefix('populate:')]
        else:
            new_dict[k] = v

    return new_dict

def do_request(backend, method, path, params={}, body={}, **kwargs):
    url = f"{backend.base_url}{path}"
    headers = create_auth(backend.token)

    print("!!!!")
    print(backend.data)

    _params = populate(backend.data, params)
    _body = populate(backend.data, body)

    if method == 'POST':
        response = requests.post(url, params=_params, json=_body, headers=headers)
    elif method == 'GET':
        response = requests.get(url, params=_params, headers=headers)

    print(url)
    print(_params)
    print(_body)
    print(response.content)
    
    if response.status_code == 200 or response.status_code == 201:
        response_data = response.json()
        response_body = json.dumps(response_data, indent=2)
    else:
        response_body = response.content
        response_data = {}

    return {
        'method': method,
        'url': url,
        'params': _params, 
        'request_body': _body,
        'status_code': response.status_code,
        'response_body': response_body,
        'response_data': response_data
    }

def run_endpoints(backends):
    results = []

    for endpoint in ENDPOINTS:
        endpoint_response = {
            'method': endpoint.method,
            'path': endpoint.path,
            'cases': []
        }
        
        for case in endpoint.cases:
            case_results = {
                'title': case.title
            }

            for backend in backends:
                result = do_request(
                    backend,
                    endpoint.method,
                    endpoint.path,
                    **case._asdict()
                )
                case_results[backend.name] = result

                response_data = result['response_data']
                if 'experiment_id' in response_data:
                    backend.data['experiment_id'] = response_data['experiment_id']
                
                if 'run' in response_data:
                    backend.data['run_id'] = response_data['run']['info']['run_id']

            endpoint_response['cases'].append(case_results)    

        results.append(endpoint_response)

    return results

if __name__ == "__main__":
    results = run_endpoints(BACKENDS)

    environment = Environment(loader=FileSystemLoader("templates/"))
    template = environment.get_template("api_parity.md")
    
    with open("results.md", "w") as f:
        f.write(template.render(endpoints=results))