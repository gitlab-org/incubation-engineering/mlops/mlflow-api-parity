MLFlow API Parity
=================

The MLFlow API Parity project provides a collection of tests that run against both the MLFlow REST API and the GitLab MLFlow REST API to ensure consistent behavior and API parity.

## Getting Started

Before getting started, ensure you have `python`, `pip`, and the GitLab GDK installed locally. Additionally, ensure that your local GDK has the `ml_experiment_tracking` feature flag enabled.

You can enable in the Rails console with:

```ruby
Feature.enable(:ml_experiment_tracking)
```

1. Clone this project locally.
1. Install the dependencies via pip `pip install -r requirements.txt`.
1. Create a .env file from the example `cp .env.example .env`.
1. Create a new project in your GDK for testing and add the project id to the .env file.
1. Create a personal access token for testing and add it to the .env file (note: it must be a personal access token, project access tokens won't work). 
1. Set your GDK host in the .env file if it is different from the example.
1. Install MLFlow `pip install mlflow`.
1. Start the MLFlow server `mlflow server`.
1. Set the MFLlow host in the .env file if it is different from the example.

## Running Tests

1. Run the tests by running `python run.py`
1. If everything worked you'll see a file called `results.md` with the output from the tests 